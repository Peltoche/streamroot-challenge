## Streamroot-challenge-server

#### Intro

This little messaging server is created to respond to a challenge from
Streamroot, a streaming service based on the P2P. The main issue in P2P
exchanges is the scalability of the tracker used to link the peers between
them. This server is a proposal to resolve this issue.


#### Issues/solutions

A Distributed Hash Table (DHT) taken from the bittorrent protocole would be
perfect for the scalability but it could lead to several other issues as speed,
metadata propagation or just control on the peers.


So it seem mandatory to create a centralized service which will need a big
vertical scalability. By consequence the services need to be as stateless as
possible.

[Schema](https://gitlab.com/Peltoche/streamroot-challenge-server/blob/master/schema.png)



### Run it

You need docker and docker-compose

```
docker-compose up

```

The web client is at the address :
```
localhost:3000
```

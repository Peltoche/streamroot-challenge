// Package router implements a basic http router. It create a lite wrapper
// around the mux package.

package router

import (
	"github.com/gorilla/mux"
	"net/http"
)

type Route struct {
	Name        string           // Internal name for commodity
	Method      string           // http method: "GET", "POST", etc.
	Pattern     string           // The address of the route: "/", "/signup", etc.
	HandlerFunc http.HandlerFunc // The handler function
}

// NewRouter creates a mux router
// Returns the router holding the routes defined by the variable "routes"
func NewRouter(routes []Route) *mux.Router {
	router := mux.NewRouter()

	var handler http.Handler
	for _, route := range routes {
		handler = Logger(route.HandlerFunc, route.Name)

		router.
			Methods(route.Method).
			Path(route.Pattern).
			Name(route.Name).
			Handler(handler)
	}

	return router
}

package router

import (
	"fmt"
	"github.com/gorilla/mux"
	"net/http"
	"testing"
)

var Routes = []Route{
	Route{
		"Index",
		"GET",
		"/",
		Index,
	},
	Route{
		"Hello",
		"GET",
		"/hello/{name}",
		Hello,
	},
}

func Index(w http.ResponseWriter, r *http.Request) {
	fmt.Fprintf(w, "Welcome!")
}

func Hello(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	w.Header().Set("Content-Type", "text/plain")
	w.WriteHeader(http.StatusOK)
	fmt.Fprintf(w, "Hello %s!", vars["name"])
}

func TestCreationRouter(t *testing.T) {
	NewRouter(Routes)
}

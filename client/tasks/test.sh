# **************************************************************************** #
#                                                                              #
#    test.sh                                                                   #
#                                                                              #
#    By: peltoche <dev@halium.fr>                                              #
#                                                                              #
#    Created: 2016/05/25                                                       #
#                                                                              #
# **************************************************************************** #


ABSPATH=$(cd "$(dirname "$0")"; pwd)
BASE_DIR="$ABSPATH/../"

./node_modules/check-build/bin/check-build
cur_rc=$?
if [ $cur_rc -eq 0 ]; then
	mocha test/**/*
fi

exit $rc

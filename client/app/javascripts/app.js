// ************************************************************************** //
//                                                                            //
//   app.js                                                                   //
//                                                                            //
//   By: peltoche <dev@halium.fr>                                             //
//                                                                            //
//   Created: 2016/05/18                                                      //
//                                                                            //
// ************************************************************************** //

const request = require("request-promise");

let trackerUrl
let clientId

function log(str) {
	const logger = document.getElementById("logger")

	const line = document.createElement("p")
	line.innerHTML = str

	logger.appendChild(line)
}

function connectTracker(provider) {
	const clientInfo = {
		provider: provider
	}

	log("Connect as youtube...")
	log("Client info:")
	log("- provider: " + provider)

	return Promise.resolve({
		method: "POST",
		url: "http://localhost:8080/connect/",
		body: clientInfo,
		json: true,
	})
	.then(request)
	.then((res) => {
		log("Assigned to tracker: " + res)
		trackerUrl = res

		log(`Connect to tracker (http://${trackerUrl}/connect/)...`)

	})
	.then(() => {
		return request({
			method: "POST",
			url: `http://${trackerUrl}/connect/`,
			json: true,
		})
	})
	.then((res) => {
		log("Client id: " + res)
		clientId = res
	})
	.catch((error) => {
		log("Error API info: " + error);
	})
}


function addOffer() {
	if (!trackerUrl) {
		log("No connected to a tracker")
		return
	}

	const offer = document.getElementById("offer").value

	if (offer.length == 0) {
		log("Need an value")
		return
	}

	log(`Add offer "${offer}" to tracker ${trackerUrl}`)

	return Promise.resolve({
		method: "POST",
		url: `http://${trackerUrl}/offer/${offer}`,
		headers: {
			"Authorization": clientId
		}
	})
	.then(request)
	.then((res) => {
		log("Success")
	})
	.catch((error) => {
		log("Error API info: " + error);
	})
}


function getOffer() {
	if (!trackerUrl) {
		log("No connected to a tracker")
		return
	}

	const offer = document.getElementById("offer").value

	if (offer.length == 0) {
		log("Need an value")
		return
	}

	log(`Get offer "${offer}" from tracker ${trackerUrl}`)

	return Promise.resolve({
		method: "GET",
		url: `http://${trackerUrl}/offer/${offer}`,
	})
	.then(request)
	.then((res) => {
		log("Success: " + res)
	})
	.catch((error) => {
		log("Error API info: " + error);
	})
}


document.getElementById("y-btn").addEventListener("click", () => {
	connectTracker("youtube")
})

document.getElementById("d-btn").addEventListener("click", () => {
	connectTracker("dailymotion")
})

document.getElementById("add-btn").addEventListener("click", addOffer)

document.getElementById("get-btn").addEventListener("click", getOffer)


const browserify = require('browserify');
const gulp = require('gulp');
const source = require('vinyl-source-stream');
const buffer = require('vinyl-buffer');
const gutil = require('gulp-util');
const uglify = require('gulp-uglify');
const sourcemaps = require('gulp-sourcemaps');
const babelify = require('babelify');
const browserSync = require('browser-sync').create();
const eslintify = require('eslintify');
const notify = require('gulp-notify');
const gulpif = require('gulp-if');
const cssPrefixer = require('gulp-autoprefixer');
const htmlmin = require('gulp-htmlmin');
const cleanCss = require('gulp-clean-css');
const replace = require('gulp-replace');
const concatCss = require('gulp-concat-css');


environment = (process.env.NODE_ENV || 'development');


const onError = (err) => {
  notify.onError({
    title:    'Error',
    message:  '<%= error %>',
  })(err);
};


/**
 * Transpile / replace / map / uglify javascript files
 */
gulp.task('javascript', function () {
  // set up the browserify instance on a task basis
  const b = browserify({
    entries: './app/javascripts/app.js',
    debug: true,
    // defining transforms here will avoid crashing your stream
    transform: [
      'eslintify',
      ['babelify', {presets: ['es2015', 'stage-0']}]],
  });


  bundle = () => {
    return b.bundle()
    .on('error', onError)
    .pipe(source('./bundle.js'))
    .pipe(buffer())
    .pipe(gulpif(environment === 'production', uglify()))
    .pipe(gulp.dest('./public/dist/js/'));
  };

  b.on('update', bundle);
  bundle();
});

const csslint = require('gulp-csslint');

/**
 * Minify css
 */
gulp.task('styles', function () {
  return gulp.src('app/styles/*.css')
  .pipe(csslint())
  .pipe(csslint.reporter())
  .pipe(cssPrefixer({
    browsers: ['> 5%'],
  }))
  .pipe(concatCss('app.css'))
  .pipe(cleanCss({compatibility: 'ie8'}))
  .pipe(gulp.dest('./public/dist/styles/'));
});


/**
 * Minify html
 */
gulp.task('html', function () {
  return gulp.src('app/html/*.html')
  .pipe(htmlmin({collapseWhitespace: true}))
  .pipe(gulp.dest('./public/'))
})

const mocha = require('gulp-mocha');

gulp.task('test', function () {
  return gulp.src('./test/**/*.js')
  .pipe(mocha());
});


/**
 * Run watcher and auto-reloader
 */
gulp.task('serve', ['javascript', 'styles', 'html'], () => {
  browserSync.init({
    server: './public'
  });

  gulp.watch('./app/javascripts/**/*.js', ['javascript']);
  gulp.watch('./app/html/*.html', ['html']);
  gulp.watch('./app/styles/*.css', ['styles']);
  gulp.watch('public/**/*').on('change', browserSync.reload);
});

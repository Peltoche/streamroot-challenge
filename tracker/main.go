package main

import (
	"gitlab.com/Peltoche/streamroot-challenge/router"
	"gopkg.in/redis.v4"
	"log"
	"net/http"
	"os"
)

var (
	httpPort      string = ":80"
	redisHost     string = "redis.streamroot.com"
	redisPort     string = "6379"
	redisPassword string = ""
)

// Clients
var (
	RedisClient *redis.Client
)

func main() {
	// Init redis client
	initRedis()

	// Init http server
	router := router.NewRouter(ApiRoutes)
	log.Println("Listen on port: ", httpPort)
	log.Fatal(http.ListenAndServe(httpPort, router))
}

// InitRedis create a new connection to the redis server with the corresponding
// env variables.
func initRedis() {
	// Create redis client
	RedisClient = redis.NewClient(&redis.Options{
		Addr:     redisHost + ":" + redisPort,
		Password: "",
		DB:       0,
	})

	// Test redis client
	_, err := RedisClient.Ping().Result()
	if err != nil {
		log.Println("Fail initialize redis: ", err)
		os.Exit(1)
	}

	log.Println("Connection with Redis ok")
}

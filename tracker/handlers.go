package main

import (
	"encoding/json"
	"errors"
	"github.com/gorilla/mux"
	"github.com/satori/go.uuid"
	"gopkg.in/redis.v4"
	"log"
	"net"
	"net/http"
)

////////////////////  STRUCTS  ////////////////////

// clientData represent all the data on a client saved on Redis.
//
// Many things need to be moved in a sql database to assure the persistency of
// the data..
type clientData struct {
	Ressources []string `json:"ressources"`
	Host       string
	Port       string
}

// ressourceData represent all the data of one ressource. It containt for now
// just a list of all clients offering this ressource.
type ressourceData struct {
	ClientOffers []string `json:"clientOffers"`
}

////////////////////  CONNECT HANDLER  ////////////////////

// ConnectHandler recieve a request from a client who want to connect to the
// tracker. That will authorize him to request pairing other peers.
func ConnectHandler(w http.ResponseWriter, req *http.Request) {
	clientKey := uuid.NewV4().String()
	var err error
	var clientData clientData

	clientData.Host, clientData.Port, err = net.SplitHostPort(req.RemoteAddr)

	log.Println("clientData: ", clientData)

	rawClientData, err := json.Marshal(&clientData)
	if err != nil {
		log.Println("Fail marshal redis ressource: ", err)
		http.Error(w, "Internal Error", 500)
		return
	}

	if err := RedisClient.Set(string(clientKey), rawClientData, 0).Err(); err != nil {
		log.Println("Fail save redis ressource: ", err)
		http.Error(w, "Internal Error", 500)
		return
	}

	w.Header().Set("Access-Control-Allow-Origin", "*")
	w.Write([]byte(clientKey))
}

// PreflightConnectHandler handle the preflight request for Connect request.
// It return the valids CORS
func PreflightConnectHandler(w http.ResponseWriter, req *http.Request) {
	w.Header().Set("Access-Control-Allow-Origin", "*")
	w.Header().Set("Access-Control-Allow-Methods", "POST")
	w.Header().Set("Access-Control-Allow-Headers", "Content-Type")
}

////////////////////  CREATE OFFER HANDLER  ////////////////////

// CreateOfferHandler handle a request from a client who want to offer an access
// to a specific ressource.
//
// For now the ressource is automatically accepted and saved but in the future
// the clients will not have authorization to create offer on unknown ressource.
//
// TODO: Check client id
func CreateOfferHandler(w http.ResponseWriter, req *http.Request) {
	var ressourceData ressourceData

	var targetId = mux.Vars(req)["targetId"]
	var clientId = req.Header.Get("Authorization")

	// Check if the ressource is present in database
	rawRessource, err := RedisClient.Get(targetId).Result()
	// If not found create a new ressource
	if err == redis.Nil {
		if err := createNewOffer(targetId, clientId); err != nil {
			log.Println("Fail to create new offer: ", err)
			http.Error(w, "Internal Error", 500)
			return
		}

		w.Header().Set("Access-Control-Allow-Origin", "*")
		return
	}
	if err != nil {
		log.Println("Fail retrieving redis ressource: ", err)
		http.Error(w, "Internal Error", 500)
		return
	}

	// If the ressource is finded, add the client to the offer list
	if err := json.Unmarshal([]byte(rawRessource), &ressourceData); err != nil {
		log.Println("Fail unmarshal ressource data: ", err)
		http.Error(w, "Internal Error", 500)
		return
	}

	// Add the client id to the ressource
	ressourceData.ClientOffers = append(ressourceData.ClientOffers, clientId)

	log.Println("create ressource: ", ressourceData)

	newRawRessourceData, err := json.Marshal(&ressourceData)
	if err != nil {
		log.Println("Fail marshal client data: ", err)
		http.Error(w, "Internal Error", 500)
		return
	}

	if err := RedisClient.Set(targetId, newRawRessourceData, 0).Err(); err != nil {
		log.Println("Fail save updated ressource data: ", err)
		http.Error(w, "Internal Error", 500)
		return
	}

	w.Header().Set("Access-Control-Allow-Origin", "*")
}

// createNewOffer is a temporary function creating a new ressource if the target
// offer is unknown.
func createNewOffer(targetId string, clientId string) error {
	ressource := ressourceData{
		[]string{
			clientId,
		},
	}

	log.Println("Create new ressource: ", ressource)

	rawRessource, err := json.Marshal(&ressource)
	if err != nil {
		log.Println("Fail marshal ressouce data: ", err)
		return err
	}

	if err := RedisClient.Set(targetId, rawRessource, 0).Err(); err != nil {
		log.Println("Fail save new ressource into redis: ", err)
		return err
	}

	if err := saveNewRessourceToClient(clientId, targetId); err != nil {
		log.Println("Fail save new ressource to client into redis: ", err)
		return err
	}

	return nil
}

// saveNewRessourceToClient update inside the client data his his list of
// ressource it offer.
func saveNewRessourceToClient(clientId string, targetId string) error {
	var clientData clientData

	rawClientData, err := RedisClient.Get(clientId).Result()
	if err != nil {
		log.Println("Fail retrieving client data: ", err)
		return err
	}

	if err := json.Unmarshal([]byte(rawClientData), &clientData); err != nil {
		log.Println("Fail unmarshal client data: ", err)
		return err
	}

	clientData.Ressources = append(clientData.Ressources, clientId)

	newRawClientData, err := json.Marshal(&clientData)
	if err != nil {
		log.Println("Fail marshal ressouce data: ", err)
		return err
	}

	if err := RedisClient.Set(clientId, newRawClientData, 0).Err(); err != nil {
		log.Println("Fail save updated client data: ", err)
		return err
	}

	return nil
}

// PreflightCreateOfferHandler handle the preflight request for Create Offer
// request. It return the valids CORS
func PreflightCreateOfferHandler(w http.ResponseWriter, req *http.Request) {
	w.Header().Set("Access-Control-Allow-Origin", "*")
	w.Header().Set("Access-Control-Allow-Methods", "POST")
	w.Header().Set("Access-Control-Allow-Headers", "Content-Type")
	w.Header().Set("Access-Control-Allow-Headers", "Authorization")
}

////////////////////  GET OFFER HANDLER  ////////////////////

// GetOfferHandler handle a request from a client who want to access to a
// specific ressource.
//
// TODO: Check client id
func GetOfferHandler(w http.ResponseWriter, req *http.Request) {
	var ressourceData ressourceData

	var targetId = mux.Vars(req)["targetId"]
	// Check if the ressource is present in database
	rawRessource, err := RedisClient.Get(targetId).Result()

	// If not found return an error
	if err == redis.Nil {
		log.Println("Try access unknown ressource: ", targetId)
		http.NotFound(w, req)
		return
	}
	if err != nil {
		log.Println("Fail retrieving redis ressource: ", err)
		http.Error(w, "Internal Error", 500)
		return
	}

	// If the ressource is found, select the best peer and return its ip
	if err := json.Unmarshal([]byte(rawRessource), &ressourceData); err != nil {
		log.Println("Fail unmarshal ressource data: ", err)
		http.Error(w, "Internal Error", 500)
		return
	}

	bestPeer, err := selectBestPeer(ressourceData)
	if err != nil {
		log.Println("Fail select best peer: ", err)
		http.Error(w, "Internal Error", 500)
		return
	}

	log.Println("client: ", bestPeer)

	bestPeerAddr := bestPeer.Host + ":" + bestPeer.Port

	w.Header().Set("Access-Control-Allow-Origin", "*")
	w.Write([]byte(bestPeerAddr))
}

// selectBestPeed take a list of offerer for a ressource and return the suitable
// for peer the ressource.
//
// For now it only choose the first one.
func selectBestPeer(ressourceData ressourceData) (*clientData, error) {
	var clientData clientData

	if len(ressourceData.ClientOffers) == 0 {
		return nil, errors.New("No client offer for ressource: ")
	}

	bestClientId := ressourceData.ClientOffers[0]

	rawClient, err := RedisClient.Get(bestClientId).Result()
	// If the client is not found, return an error
	if err == redis.Nil {
		return nil, errors.New("Client data not found: " + bestClientId)
	}
	if err != nil {
		return nil, errors.New("Fail retrieve client data: " + bestClientId)

	}

	// If the client is found, return its ip
	if err := json.Unmarshal([]byte(rawClient), &clientData); err != nil {
		return nil, err
	}

	return &clientData, nil
}

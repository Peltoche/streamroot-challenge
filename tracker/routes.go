package main

import (
	"gitlab.com/Peltoche/streamroot-challenge/router"
)

var ApiRoutes = []router.Route{
	router.Route{
		"Connect",
		"POST",
		"/connect/",
		ConnectHandler,
	},
	router.Route{
		"CORSConnect",
		"OPTIONS",
		"/connect/",
		PreflightConnectHandler,
	},

	router.Route{
		"CreateOffer",
		"POST",
		"/offer/{targetId}",
		CreateOfferHandler,
	},
	router.Route{
		"CORSCreateOffer",
		"OPTIONS",
		"/offer/{targetId}",
		PreflightCreateOfferHandler,
	},

	router.Route{
		"GetOffer",
		"GET",
		"/offer/{targetId}",
		GetOfferHandler,
	},
	router.Route{
		"CORSGetOffer",
		"OPTIONS",
		"/offer/{targetId}",
		PreflightCreateOfferHandler,
	},
}

package main

import (
	"encoding/json"
	"log"
	"net/http"
)

// connectBody represent the body content of the Contect request. For now there
// is only fake data and unused data.
// The next step is to add other metadata used to choose more precisely on
// which tracker it will redirect the client. It could be:
// - Geo		- To enable geo-localised tracker (france/asia/...)
// - Provider	- To enable tracker by provider (youtube.com/daylimotion.com/...)
// - Platform	- To redirect platform-specific tracker (phone/windows/...)
// - etc...
type connectBody struct {
	RessourceId string `json:"ressourceId"`
	Provider    string `json:"provider"`
	DataType    string `json:"dataType"`
}

// ConnectHandler recieve a request from a client who want to connect to a
// tracker. This client provide a set of personnal data so it can
// redirect it to the correct tracker. Each tracker need to have it's own ip
// or specific port.
func ConnectHandler(w http.ResponseWriter, req *http.Request) {
	var body connectBody
	var trackerUrl string

	if err := json.NewDecoder(req.Body).Decode(&body); err != nil {
		log.Printf("Error decoding json: %+v", err)
		http.Error(w, "Invalid json", 400)
		return
	}

	if trackerUrl = findTracker(body); len(trackerUrl) == 0 {
		log.Println("Fail find correct provider: ", body.Provider)
		http.Error(w, "Invalid request", 400)
		return
	}

	w.Header().Set("Access-Control-Allow-Origin", "*")
	w.Write([]byte(trackerUrl))
}

// findTracker return the correct tracker url based on the request content
//
// In the current state the rules are hardcoded but in the future they would be
// dynamique and include a load-balancer.
func findTracker(body connectBody) string {
	switch {
	case body.Provider == "youtube":
		return "localhost:8081"
	case body.Provider == "dailymotion":
		return "localhost:8082"
	default:
		return ""
	}
}

// PreflightConnectHandler handle the preflight request for Connect. It return
// the valids CORS
func PreflightConnectHandler(w http.ResponseWriter, req *http.Request) {
	w.Header().Set("Access-Control-Allow-Origin", "*")
	w.Header().Set("Access-Control-Allow-Methods", "POST")
	w.Header().Set("Access-Control-Allow-Headers", "Content-Type")
}

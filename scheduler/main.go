package main

import (
	"gitlab.com/Peltoche/streamroot-challenge/router"
	"log"
	"net/http"
)

var (
	httpPort string = ":80"
)

func main() {
	// Init http server
	router := router.NewRouter(ApiRoutes)
	log.Println("Listen on port: ", httpPort)
	log.Fatal(http.ListenAndServe(httpPort, router))
}

package main

import (
	"gitlab.com/Peltoche/streamroot-challenge/router"
)

var ApiRoutes = []router.Route{
	router.Route{
		"Connect",
		"POST",
		"/connect/",
		ConnectHandler,
	},
	router.Route{
		"CORS",
		"OPTIONS",
		"/connect/",
		PreflightConnectHandler,
	},
}

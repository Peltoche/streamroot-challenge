package main

import (
	"testing"
)

func TestFindTrackerYoutube(t *testing.T) {
	body := connectBody{
		"kitten.video",
		"youtube",
		"video",
	}

	trackerUrl := findTracker(body)
	if trackerUrl != "localhost:8081" {
		t.Error("Bad tracker url returned")
	}
}

func TestFindTrackerDaylimotion(t *testing.T) {
	body := connectBody{
		"kitten.video",
		"dailymotion",
		"video",
	}

	trackerUrl := findTracker(body)
	if trackerUrl != "localhost:8082" {
		t.Error("Bad tracker url returned")
	}
}
func TestFindTrackerError(t *testing.T) {
	body := connectBody{
		"kitten.video",
		"badProvider",
		"video",
	}

	trackerUrl := findTracker(body)
	if len(trackerUrl) != 0 {
		t.Error("Should return empty url")
	}
}
